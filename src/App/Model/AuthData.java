package App.Model;

import App.Public.Util.CipherUtil;

import java.io.*;
import java.util.HashMap;

/**
 * 用户列表
 */
public class AuthData implements Serializable {
    //用户名在此列表统一小写
    //authBook 键为用户名，值为密码,用于身份验证
    private  HashMap<String,String> authBook=new HashMap<>();

    //DataBook 键为用户名，值为用户数据类的对象，用于登录后加载对应的数据
    //private  HashMap<String,String> DataBook=new HashMap<>();

    /**
     * 检测用户密码是否匹配
     * @param username 用户名
     * @param psw 密码
     * @return 验证结果：TRUE:正确 FALSE:错误
     */
    public boolean checkPsw(String username,String psw) {
        username = username.toLowerCase();
        if(authBook.containsKey(username))
            //return authBook.get(username).equals(psw);
            //验证加密后的密码与输入的密码是否匹配
            return CipherUtil.validatePassword(authBook.get(username),psw);
        else
            return false;
    }

    /**
     * 检测用户名是否已经被注册
     * @param username 用户名
     * @return    如果被注册会返回true，否则返回false
     */
    public boolean isExist(String username) {
        username = username.toLowerCase();
        return authBook.containsKey(username);
    }

    /**
     * 注册函数
     * @param username 用户名
     * @param psw 密码
     * @return 如果成功注册会返回true
     * @throws Exception  如果用户已经注册，会返回异常
     */
    public boolean register(String username,String psw) throws Exception {
        username = username.toLowerCase();
        if(authBook.containsKey(username))
            throw new Exception("该用户已经存在");
        else {
            if(!createUserFile(username)) throw new Exception("注册出现异常");
            authBook.put(username,psw);   //在账号密码本里面注册
            UserData newUser=new UserData(username);
            newUser.save();
            newUser.setAndSaveConflict("电话","true");
            newUser.setAndSaveConflict("邮箱","true");
            //DataBook.put(username,newUser.getDataPath());
            //save();
            return true;
        }
    }

    /**
     * 用于返回用户名对应的用户数据对象
     * @param username 用户名
     * @return 返回用户名对应的用户数据对象
     * @throws Exception 如果用户不存在会返回异常
     */
    public UserData getUserData(String username) throws Exception {
        username = username.toLowerCase();
        if(!isExist(username))
            throw new Exception("不存在该用户");
        else
        {
            String dataPath=System.getProperty("user.dir")+"/userFile/"+username+"/data/"+username+".dat";
            if(!(new File(dataPath)).exists())
            {
                throw new Exception("不存在用户数据");
            }
            else
            {
                ObjectInputStream output = new ObjectInputStream(new FileInputStream(dataPath));
                return (UserData)output.readObject();
            }
        }
    }

    public UserData testUserData(String username) throws Exception {
        //测试用
        username = username.toLowerCase();
        String dataPath=System.getProperty("user.dir")+"/userFile/"+username+"/data/"+username+".dat";
        if(!(new File(dataPath)).exists())
        {
            throw new Exception("不存在用户数据");
        }
        else
        {
            ObjectInputStream output = new ObjectInputStream(new FileInputStream(dataPath));
            return (UserData)output.readObject();
        }
    }

    private boolean createUserFile(String username) {

        username = username.toLowerCase();
        String basePath=System.getProperty("user.dir")+"/userFile/"+username;
        if(!(new File(basePath)).exists())
        {   //如果路径不存在的话就创建文件夹
            //data文件夹用于存储用户的数据
            //image文件夹用于存储用户的图片信息
            (new File(basePath+"/data")).mkdirs();
            (new File(basePath+"/image")).mkdirs();
        }
        return true;
    }

    public AuthData getData() throws Exception {

        String dataPath=System.getProperty("user.dir")+"/userFile/"+"auth/authData.dat";
        File file = new File(dataPath);
        //System.out.println(file.getAbsolutePath()); //寻找文件路径
        if(!file.exists())
        {
            AuthData initData=new AuthData();
            initData.save();
            return initData;
        }
        else
        {
            ObjectInputStream output = new ObjectInputStream(new FileInputStream(dataPath));
            return (AuthData)output.readObject();
        }
    }

    public void save() throws Exception
    {
//        String path = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
//        path = java.net.URLDecoder.decode(path, "UTF-8");
//        String dataPath = path + "/userFile/" + "auth/authData.dat";
        String dataPath = System.getProperty("user.dir") + "/userFile/" + "auth/authData.dat";
        File file = new File(dataPath.replace("/authData.dat",""));
        //System.out.println(file.getAbsolutePath()); //寻找文件创建路径
        if (!file.exists()){
            file.mkdirs();
        }
        AuthData item=this;
        ObjectOutputStream output =new ObjectOutputStream(new FileOutputStream(dataPath));
        output.writeObject(item);
    }

    /**
     * 修改密码
     * @param user 用户名
     * @param password  密码
     * @throws Exception    用户不存在时会丢出异常
     */
    public void modifyPassword(String user,String password) throws Exception
    {
        if(!isExist(user))
        {
            throw new Exception("该用户名不存在");
        }
        else
        {
            this.authBook.put(user,password);
        }
    }


}