package App.Model;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;

/**
 * To save contacts's message.
 */
public class Contact_Message implements Serializable {
    private URI picture = null; //照片
    private String giveName = ""; //姓氏
    private String firstName = ""; //名字
    private String fullName = "";  //全名
    private ArrayList<String> phoneNumber = new ArrayList<>(); //电话号码(可能有多个)
    private ArrayList<String> Email = new ArrayList<>(); //邮箱地址(可能有多个)
    private String remark = ""; //备注
    private String sex = ""; //性别
    private String birthday = ""; //生日
    private String QQ = ""; //QQ
    private String petName = ""; //昵称
    private String website = ""; //网站
    /*
    住址:国家 省 市 街道 邮政编码
     */
    private String country_Home = "";
    private String province_Home = "";
    private String city_Home = "";
    private String street_Home = "";
    private String postCode_Home = "";
    private String company = "";     //公司
    private String department = "";  //部门
    private String job = "";         //职位
    private String fax = "";          //传真
    /*
    公司地址:国家 省 市 街道 邮政编码
     */
    private String country_Company = "";
    private String province_Company = "";
    private String city_Company = "";
    private String street_Company = "";
    private String postCode_Company = "";
    /*
    分组和用户归属信息
     */
    private ArrayList<String> group = new ArrayList<>(); //分组归属
    private String userName = ""; //用户归属

    private boolean isStar=false;


    /**
     * 构造器
     * 用户、分组归属信息初始化
     *
     * @param userName 用户名
     * @param groupName 分组名
     */
    public Contact_Message(String userName,String groupName){
        this.userName = userName;  //用户归属初始化
        this.group.add(groupName); //分组初始化
    }

    /**
     * 构造器
     * 未分组联系人新建及用户归属信息初始化
     * @param userName 用户名
     */
    public Contact_Message(String userName){
        this.userName = userName;  //用户归属初始化
    }

    public Contact_Message(){
    }       //此构造器应该在完善功能后删去，用含参构造器代替

    /*
    add
     */
    public void addGroup(String groupName){
        this.group.add(groupName);
    }

    public void addPhoneNumber(String phoneNum){
        this.phoneNumber.add(phoneNum);
    }

    public void addEmail(String mail){
        this.Email.add(mail);
    }

    public String toString(){
        String phoneExample = "";
        if (phoneNumber.size() != 0)
            phoneExample = " 联系电话 " + phoneNumber.get(0);
        return getFullName() + phoneExample;
    }


    /*
    get方法
     */

    public boolean getIsStart()
    {
        return this.isStar;
    }

    public String getFullName(){return this.fullName;}

    public URI getPicture() { return picture; }

    public String getBirthday() { return birthday; }

    public String getFirstName() { return firstName; }

    public String getCity_Home() { return city_Home; }

    public String getCompany() { return company; }

    public String getGiveName() {return giveName; }

    public String getCountry_Home() { return country_Home; }

    public String getPetName() { return petName; }

    public String getDepartment() { return department; }

    public String getPostCode_Home() { return postCode_Home; }

    public String getJob() { return job; }

    public String getProvince_Home() { return province_Home; }

    public String getFax() { return fax; }

    public String getQQ() { return QQ; }

    public String getRemark() { return remark; }

    public String getSex() { return sex; }

    public String getCountry_Company() { return country_Company; }

    public String getStreet_Home() { return street_Home; }

    public String getWebsite() { return website; }

    public ArrayList<String> getEmailList() { return Email; }

    public ArrayList<String> getPhoneNumberList() { return phoneNumber; }

    public String getCity_Company() { return city_Company; }

    public ArrayList<String> getGroup() { return group; }

    public String getPostCode_Company() { return postCode_Company; }

    public String getProvince_Company() { return province_Company; }

    public String getStreet_Company() { return street_Company; }

    public String getHomeAddress(){
        return country_Home + province_Home + city_Home + street_Home;
    }

    public String getUserName() { return userName; }


    /*
    set方法
     */

    /**
     * 修改一个分组归属
     * @param oldGroupName 旧的分组归属（String表示）
     * @param newGroupName 新的分组归属（String表示）
     * @return 修改成功返回true，否则修改不成功返回false
     */
    public boolean modifyGroup(String oldGroupName, String newGroupName) {
        if (group.contains(oldGroupName)){
            group.set(group.indexOf(oldGroupName),newGroupName);
            return true;
        }
        else return false;
    }

    /**
     * 删除小组
     * @param name 组名
     */
    public void removeGroup(String name)
    {
        getGroup().remove(name);
    }

    /**
     * 判断分组是否为空
     * @return 分组为空则返回true，否则不为空返回false
     */
    public boolean isEmptyGroup() {
        return getGroup().isEmpty();
    }

    /**
     * 判断是否含有某个分组
     * @param groupName 分组名
     * @return 包含则返回true，否则不包含则返回false
     */
    public boolean hasGroup(String groupName)
    {
        return getGroup().contains(groupName);
    }

    public void setGroup(String g)
    {
        if(!this.group.contains(g))
        {
            this.group.add(g);
        }
    }
    public void setGroup(ArrayList<String> newGroup)
    {
        this.group=newGroup;
    }

    public void setFullName(String fullName){this.fullName=fullName;}

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setCity_Home(String city_Home) {
        this.city_Home = city_Home;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setCity_Company(String city_Company) {
        this.city_Company = city_Company;
    }

    public void setCountry_Company(String country_Company) {
        this.country_Company = country_Company;
    }

    public void setCountry_Home(String country_Home) {
        this.country_Home = country_Home;
    }

    public void setEmail(ArrayList<String> email) {Email = email;}

    public void setEmail(String email)
    {
        if(!this.Email.contains(email))
        {
            this.Email.add(email);
        }
    }
    public void setDepartment(String department) {
        this.department = department;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setGiveName(String giveName) {
        this.giveName = giveName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public void setPhoneNumber(ArrayList<String> phoneNumber) {this.phoneNumber = phoneNumber;}

    public void setPhoneNumber(String numbers)
    {   //添加电话号码
        if(!this.phoneNumber.contains(numbers))
        {
            this.phoneNumber.add(numbers);
        }
    }
    public void setJob(String job) {
        this.job = job;
    }

    public void setPicture(URI picture)
    {
        this.picture = picture;
    }

    public void setPostCode_Home(String postCode_Home) {
        this.postCode_Home = postCode_Home;
    }

    public void setPostCode_Company(String postCode_Company) {
        this.postCode_Company = postCode_Company;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setProvince_Company(String province_Company) {
        this.province_Company = province_Company;
    }

    public void setProvince_Home(String province_Home) {
        this.province_Home = province_Home;
    }

    public void setQQ(String QQ) {
        this.QQ = QQ;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setStreet_Home(String street_Home) {
        this.street_Home = street_Home;
    }

    public void setStreet_Company(String street_Company) {
        this.street_Company = street_Company;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setStar(boolean o)
    {
        this.isStar=o;
    }
}

