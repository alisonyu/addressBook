package App.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 分组Group
 */
public class Contact_Group implements Serializable
{
    private String groupName; //组名
    private ArrayList<Contact_Message> group = new ArrayList<>();
    private String user; //用户归属

    public Contact_Group(String userName,String group){
        groupName = group;
        user = userName;
    }

    public String getGroupName() {return groupName;}

    public ArrayList<Contact_Message> getGroup() { return group; }

    public void removePerson(Contact_Message o)
    {
        //双向删除
        o.removeGroup(this.groupName);
        this.group.remove(o);
    }

    /**
     * 修改组名
     * @param newGroupName 新组名
     */
    public void setGroupName(String newGroupName) {
        for(Contact_Message contact:group){
            contact.modifyGroup(this.groupName,newGroupName); //修改Group中每一个联系人的此组组名
        }
        this.groupName = newGroupName; //修改Group类自己的组名
    }

    public boolean addMember(Contact_Message person)
    {
        return this.group.add(person);
    }
}