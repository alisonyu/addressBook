package App.Public.UI;

import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;

/**
 * Created by alisonyu on 17-2-1.
 */
public class StarButton
{
    private Button startbt=new Button();
    private SimpleBooleanProperty isSelected=new SimpleBooleanProperty(false);
    private String initClass="starButton";
    private String selectedClass="selectedStarButton";
    private Handler clickHandler=null;


    public void StarButton()
    {
        initStarButton();
    }
    /**
     * 对星标按钮进行样式和基本逻辑的初始化
     */
    public void initStarButton()
    {
        if(clickHandler==null)
        {
            clickHandler=new Handler() {
                @Override
                public void run() {

                }
            };
        }
        startbt.getStyleClass().add("commonbt");
        startbt.getStyleClass().add(initClass);
        //点击会使selected属性值改变
        startbt.setOnAction(event -> {
            setSelected(!isSelected());
            clickHandler.run();
        });
        //点击导致样式发生变化，样式定义在全局css--index.css那里
        isSelected.addListener((observable, oldValue, newValue) ->
                {
                    if(newValue==true)
                    {
                        startbt.getStyleClass().remove(initClass);
                        startbt.getStyleClass().add(selectedClass);
                    }
                    else
                    {
                        startbt.getStyleClass().remove(selectedClass);
                        startbt.getStyleClass().add(initClass);
                    }
                }
        );
    }

    /**
     * 在原基础上指定点击事件
     * @param handler 点击事件的处理
     */
    public StarButton(Handler handler)
    {
        clickHandler=handler;
        initStarButton();
    }

//    /**
//     * 指定属性值变化事件
//     * @param changeHandler 属性值改变事件的处理
//     */
//    public StarButton(ChangeHandler changeHandler)
//    {
//        //属性值改变的事件
//        isSelected.addListener((observable, oldValue, newValue) -> {
//            changeHandler.run(oldValue,newValue);
//        });
//    }
//
//    /**
//     * 指定点击和属性值变化事件
//     * @param handler 点击事件处理
//     * @param changeHandler 属性值改变事件处理
//     */
//    public StarButton(Handler handler, ChangeHandler changeHandler)
//    {
//        //按钮点击事件
//        startbt.setOnAction(event -> {
//            handler.run();
//        });
//        //属性值改变的事件
//        isSelected.addListener((observable, oldValue, newValue) -> {
//            changeHandler.run(oldValue,newValue);
//        });
//    }



    public ObservableValue<Button> getStartButton()
    {
        return new ObservableValue<Button>() {
            @Override
            public void addListener(ChangeListener<? super Button> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super Button> listener) {

            }

            @Override
            public Button getValue() {
                return startbt;
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }

    public Button getStartbt()
    {
        return startbt;
    }
    public void addListenToStart(ChangeListener listener)
    {
        isSelected.addListener(listener);
    }
    /**
     * 获取是否被选中
     * @return
     */
    public Boolean isSelected()
    {
        return isSelected.getValue();
    }

    /**
     * 设定选中
     * @param o
     */
    public void setSelected(Boolean o)
    {
        isSelected.set(o);
    }

}
