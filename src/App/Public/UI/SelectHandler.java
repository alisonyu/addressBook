package App.Public.UI;

/**
 * Created by alisonyu on 17-1-23.
 */
public interface SelectHandler {
    public void run(String selectedItem);
}
