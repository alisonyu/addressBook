package App.Public.UI;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by alisonyu on 17-1-23.
 */
public class SimpleDialog
{
    /**
     * 返回一个警告提示框
     * @param errorInfo 警告内容
     * @return
     */
    public static Alert simpleErrorDialog(String errorInfo)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("警告");
        alert.setHeaderText("");
        alert.setContentText(errorInfo);
        return alert;
    }

    /**
     * 生成一个确认框
     * @param Info 确认内容
     * @param OkAction  确认后执行的动作
     * @return
     */
    public static Alert simpleConfirmDialog(String Info, Handler OkAction)
    {
        Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("请确认操作");
        alert.setHeaderText("注意");
        alert.setContentText(Info);
        Optional<ButtonType> option=alert.showAndWait();
        if(option.get()==ButtonType.OK)
        {
            OkAction.run();
        }
        return alert;
    }

    /**
     * 生成选择框
     * @param items 选择列表
     * @param content  提示内容
     * @param okAction 选择并确定后的动作
     * @return
     */
    public static ChoiceDialog<String> simpleChoiceDialog(ArrayList<String> items, String content, SelectHandler okAction)
    {
        ChoiceDialog<String> dialog=new ChoiceDialog<>(items.get(0),items);
        dialog.setTitle("请选择");
        dialog.setHeaderText("请选择你要进行的操作");
        dialog.setContentText(content);
        Optional<String>result =dialog.showAndWait();
        if(result.isPresent())
        {
            okAction.run(result.get());
        }
        return dialog;
    }
}


