package App.Public.UI;

/**
 * Created by alisonyu on 17-2-1.
 */
public interface ChangeHandler {
    public<E> void run(E oldValue,E newValue);
}
