package App.Public;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

public class function {
    /**
     * 返回APP路径
     * @return 路径
     */
    public final String getroot() throws UnsupportedEncodingException {
        URL U=getClass().getClassLoader().getResource("App");
        String s=U.toString();
        s=s.substring(s.indexOf('/'),s.length());
        s=URLDecoder.decode(s,"UTF-8");
        return s;
    }
    public final String getroot(String path) throws UnsupportedEncodingException {
        URL U=getClass().getClassLoader().getResource("App/"+path);
        String s=U.toString();
        s=s.substring(s.indexOf('/'),s.length());
        s=URLDecoder.decode(s,"UTF-8");
        return s;

    }
    /**
     * 加载资源文件
     * @return 路径
     */
    public final URL getResource(String name)
    {
        return getClass().getClassLoader().getResource("App/Resource/"+name);
    }

    /**
     *
     * @param viewName 在view层的fxml路径名
     * @return  返回parent对象
     * @throws IOException
     */
    public final Parent loadXML(String viewName) throws IOException
    {
        URL url=getClass().getClassLoader().getResource("App/View/"+viewName);
        return FXMLLoader.load(url);
    }

    /**
     * 加载FXML并显示新窗口
     * @param title 窗口的标题
     * @param viewName fxml文件的名字
     * @throws IOException
     */
    public final void showXML(String title,String viewName) throws IOException
    {
        Stage stage=new Stage();
        stage.setResizable(false);
        stage.setTitle(title);
        stage.setScene(new Scene(loadXML(viewName)));
        loadInitCss(stage.getScene());
        stage.show();
    }

    public final Stage showXML(String title,String viewName,Stage stage) throws IOException
    {
        stage.setTitle(title);
        stage.setResizable(false);
        stage.setScene(new Scene(loadXML(viewName)));
        loadInitCss(stage.getScene());
        stage.show();
        return stage;
    }

    /**
     * 加载APP主页面，并且会更新Stage信息到数据仓库中
     * @param title 标题
     * @param viewName FXML地址
     * @param stage 舞台
     * @throws Exception
     */
    public final void showAPP(String title,String viewName,Stage stage) throws Exception
    {
        stage.setTitle(title);
        saveAppStage(stage);
        stage.setResizable(false);
        stage.setScene(new Scene(loadXML(viewName)));
        loadInitCss(stage.getScene());
        stage.centerOnScreen();
        stage.show();
    }

    public final void showAPP(String title,String viewName) throws Exception
    {
        Stage stage=new Stage();
        saveAppStage(stage);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.setScene(new Scene(loadXML(viewName)));
        loadInitCss(stage.getScene());
        stage.show();
    }

    public final void showAPPwithNoBar(String title,String viewName) throws Exception
    {
        Stage stage=new Stage();
        stage.initStyle(StageStyle.TRANSPARENT);
        //controller.DATA.register("appStage",stage);
        saveAppStage(stage);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.setScene(new Scene(loadXML(viewName)));
        loadInitCss(stage.getScene());
        stage.show();
    }
    /**
     *
     * @param node   要添加CSS的控件
     * @param cssName CSS文件名，要放置在Resource/CSS内
     */
    public final void loadCss(Parent node, String cssName)
    {
        node.getStylesheets().add(getResource("CSS/"+cssName).toString());
    }
    public final void loadInitCss(Scene scene,String path)
    {
        scene.getStylesheets().add(String.valueOf(getResource(path)));
    }
    public final void loadInitCss(Scene scene)
    {
        scene.getStylesheets().add(String.valueOf(getResource("CSS/index.css")));
    }
    /**
     *
     * @param Path App路径下序列化文件的相对路径
     * @return   二进制对象
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public final Object LoadData(String Path) throws IOException, ClassNotFoundException {
        ObjectInputStream input =new ObjectInputStream(new FileInputStream(getroot()+'/'+Path));
        return input.readObject();
    }

    private final void saveAppStage(Stage s) throws Exception
    {
        controller.DATA.register("appStage");
        controller.DATA.putData("appStage",s);
    }

    /**
     * 根据Key读取配置文件
     * @param key 配置key
     * @return  对应的属性
     * @throws IOException
     */
    public final String getConflict(String key) throws IOException
    {
        Properties pps = new Properties();
        pps.load(new FileInputStream(getroot("/Conflict/conflict.properties")));
        return pps.getProperty(key);
    }

    public final void setConflict(String key,String value) throws IOException {
        String ConflictPath=getroot("/Conflict/conflict.properties");
        Properties pps = new Properties();
        pps.load(new FileInputStream(ConflictPath));
        pps.put(key,value);
        OutputStream out = new FileOutputStream(ConflictPath);
        pps.store(out, "Update " + key + " name");
    }





}
