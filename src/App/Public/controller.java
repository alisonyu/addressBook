package App.Public;

import javafx.stage.Stage;
/**
 *
 */
public class controller extends function
{

    //数据仓库，用于控制器之间的数据的交互
    public static dataStore DATA=new dataStore();
    public controller() throws Exception
    {
        //数据的注册请放在该构造函数中
        //注册数据后请注释数据的用途

        DATA.register("appStage");          //存放App界面的Stage
        DATA.register("userData");          //存放登录后的userData
    }

    //获得App界面的Stage
    public final Stage getAppStage() throws Exception
    {
        return (Stage)DATA.getData("appStage");
    }
    String[] data={
            "appStage", //存放App界面的Stage
            "NowUser",   //存放当前
    };

}

