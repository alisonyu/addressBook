package App.Public;

import java.util.HashMap;

/**
 * Created by yu on 2016/12/26.
 */
public class dataStore {

    private HashMap<String,Object> dataMap =new HashMap<String,Object>();

    /**
     * 数据要先注册后使用，避免数据的覆盖
     * @param s 要注册的变量的名字
     * @param o 要存储的数据（注取出来可能要进行类型的转换）
     * @throws Exception
     */
    public void register(String s,Object o) throws Exception
    {
        if(dataMap.containsKey(s))
            return;
        else
            dataMap.put(s,o);
    }

    /**
     * 数据要先注册后使用，避免数据的覆盖
     * @param s 要注册的变量的名字
     * @throws Exception
     */
    public void register(String s) throws Exception {

        if(dataMap.containsKey(s)) {
            return;
        }
        else {
            dataMap.put(s,new Object());
        }
    }



    /**
     *
     * @param s 数据变量名
     * @param o 要存储的数据
     * @throws Exception
     */
    public void putData(String s, Object o) throws Exception {

        if(!dataMap.containsKey(s)) {
            throw new Exception("The Key should be registered first");
        }
        else {
            dataMap.put(s,o);
        }
    }

    /**
     * 移除以及注册的数据变量
     * @param s 数据变量名
     */
    public void removedata(String s) {
        dataMap.remove(s);
    }

    /**
     *
     * @param s 数据变量名
     * @return 获取存储数据的变量
     * @throws Exception
     */
    public Object getData(String s) throws Exception {

        if(!dataMap.containsKey(s)) {
            throw new Exception("The Key should be registered first");
        }
        else {
            return dataMap.get(s);
        }
    }

    /**
     * 修改对应key数据的值
     * @param key
     * @param newData 新值
     * @throws Exception
     */
    public void setData(String key,Object newData) throws Exception {
        if (!dataMap.containsKey(key))
            throw new Exception("The Key should be registered first");
        else {
            dataMap.put(key,newData); //如果该映射以前包含了一个该键的映射关系，则旧值被替换
        }
    }
}
