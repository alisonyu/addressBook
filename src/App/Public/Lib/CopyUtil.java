package App.Public.Lib;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 该类用于将一个对象的属性值更新给另外一个对象(用于数据更新)
 * 注意:更新的对象一定要有对应的getter和setter方法，不支持Map,List,Set类型属性的更新
 * Created by yu on 2017/1/2.
 */
public class CopyUtil {
    public static Object getBean(Object TargetBean, Object SourceBean) throws Exception {
        if (TargetBean == null)
            return null;
        Field[] tFields = TargetBean.getClass().getDeclaredFields();
        Field[] sFields = SourceBean.getClass().getDeclaredFields();
        try {
            for (Field field : tFields) {
                String fieldName = field.getName();
                if (fieldName.equals("serialVersionUID"))
                    continue;
                if (field.getType() == Map.class)
                    continue;
                if (field.getType() == Set.class)
                    continue;
                if (field.getType() == List.class)
                    continue;
                for (Field sField : sFields) {
                    if (!sField.getName().equals(fieldName)) {
                        continue;
                    }
                    Class type = field.getType();

                    String setName = getSetMethodName(fieldName);

                    Method tMethod = TargetBean.getClass().getMethod(setName, type);

                    String getName = getGetMethodName(fieldName);

                    Method sMethod = SourceBean.getClass().getMethod(getName, (Class<?>) null);

                    Object setterValue = sMethod.invoke(SourceBean, (Object) null);

                    tMethod.invoke(TargetBean, setterValue);
                }
            }
        } catch (Exception e) {
            throw new Exception("设置参数信息发生异常", e);
        }
        return TargetBean;
    }
    private static String getGetMethodName(String fieldName) {
        fieldName = replaceFirstCharToUpper(fieldName);
        return "get" + fieldName;
    }

    private static String getSetMethodName(String fieldName) {
        fieldName = replaceFirstCharToUpper(fieldName);
        return "set" + fieldName;
    }

    private static String replaceFirstCharToUpper(String fieldName) {
        char[] chars = new char[1];
        chars[0] = fieldName.charAt(0);
        String temp = new String(chars);
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            fieldName = fieldName.replaceFirst(temp, temp.toUpperCase());
        }
        return fieldName;
    }
}
