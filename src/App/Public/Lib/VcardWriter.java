package App.Public.Lib;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 导出vcard
 */
public class VcardWriter
{
    private ArrayList<String> data=new ArrayList<>();
    public VcardWriter()
    {
        //vcard的固定格式
        data.add("BEGIN:VCARD");
        data.add("VERSION:2.1");
        data.add("END:VCARD");
    }
    //写出文件
    public void writeVcard(String path) throws IOException
    {
        FileWriter fw=new FileWriter(path,true);
        for(String item:data)
        {
            fw.append(item+'\n');
        }
        fw.close();

    }
    //读入vacrd格式文件
    public void readVcard()
    {

    }
    public void addData(String key,String value)
    {
        data.add(data.size()-1,getKeyValue(key,value));
    }
    public void addData(String item)
    {
        data.add(data.size()-1,item);
    }
    public String getKeyValue(String key,String value,String separator)
    {
        return key+separator+value;
    }
    public String getKeyValue(String key,String value)
    {
        //以；为默认分割符号
        return getKeyValue(key,value,":");
    }
    public void addEmail(String email)
    {
        String key="EMAIL";
        addData(key,email);
    }
    public void addPhone(String phone)
    {
        String key="TEL";
        addData(key,phone);
    }
    public void addFax(String Fax)
    {
        String key="TEL;FAX";
        addData(key,Fax);
    }
    public void addAddress(String country,String provice,String city,String street,String postCode)
    {
        String key="ADR;HOME";
        String value=";;"+street+";"+city+";"+provice+";"+postCode+";"+country;
        addData(key,value);
    }
    public void addFullName(String fullName)
    {
        String key="FN";
        addData(key,fullName);
    }
    public void addName(String givenName,String firstName)
    {
        String key="N";
        String value=givenName+";"+firstName+";;;";
        addData(key,value);
    }
    public void addNickName(String nickName)
    {
        String key="NICKNAME";
        addData(key,nickName);
    }
    public void addCompanyAddress(String country,String provice,String city,String street,String postCode)
    {
        String key="ADR;WORK";
        String value=";;"+street+";"+city+";"+provice+";"+postCode+";"+country;
        addData(key,value);
    }
    public void addJob(String job)
    {
        String key="ROLE";
        addData(key,job);
    }
    public void addTitle(String title)
    {
        String key="TITLE";
        addData(key,title);
    }
    public void addCompany(String company)
    {
        String key="ORG";
        addData(key,company);
    }
    public void addBirthday(String birthday)
    {
        String key="BDAY;DATE";
        addData(key,birthday);
    }
    public void addSex(String sex)
    {
        String key="GENDER";
        String value=sex;
        switch(sex)
        {
            case "男":
                value="M";
                break;
            case "女":
                value="F";
                break;
        }
        addData(key,value);
    }
    public void addQQ(String qq)
    {
        String key="X-QQ";
        addData(key,qq);
    }


}
