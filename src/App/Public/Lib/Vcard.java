package App.Public.Lib;

import java.util.ArrayList;

/**
 * Created by alisonyu on 17-2-2.
 */
public class Vcard
{
    private String giveName = ""; //姓氏
    private String firstName = ""; //名字
    private String fullName = "";  //全名
    private ArrayList<String> phoneNumber = new ArrayList<>(); //电话号码(可能有多个)
    private ArrayList<String> Email = new ArrayList<>(); //邮箱地址(可能有多个)
    private String remark = ""; //备注
    private String sex = ""; //性别
    private String birthday = ""; //生日
    private String QQ = ""; //QQ
    private String petName = ""; //昵称
    private String website = ""; //网站
    /*
    住址:国家 省 市 街道 邮政编码
     */
    private String country_Home = "";
    private String province_Home = "";
    private String city_Home = "";
    private String street_Home = "";
    private String postCode_Home = "";
    private String company = "";     //公司
    private String department = "";  //部门
    private String job = "";         //职位
    private String fax = "";          //传真
    /*
    公司地址:国家 省 市 街道 邮政编码
     */
    private String country_Company = "";
    private String province_Company = "";
    private String city_Company = "";
    private String street_Company = "";
    private String postCode_Company = "";


    public String getFullName(){return this.fullName;}


    public String getBirthday() { return birthday; }

    public String getFirstName() { return firstName; }

    public String getCity_Home() { return city_Home; }

    public String getCompany() { return company; }

    public String getGiveName() {return giveName; }

    public String getCountry_Home() { return country_Home; }

    public String getPetName() { return petName; }

    public String getDepartment() { return department; }

    public String getPostCode_Home() { return postCode_Home; }

    public String getJob() { return job; }

    public String getProvince_Home() { return province_Home; }

    public String getFax() { return fax; }

    public String getQQ() { return QQ; }

    public String getRemark() { return remark; }

    public String getSex() { return sex; }

    public String getCountry_Company() { return country_Company; }

    public String getStreet_Home() { return street_Home; }

    public String getWebsite() { return website; }

    public ArrayList<String> getEmailList() { return Email; }

    public ArrayList<String> getPhoneNumberList() { return phoneNumber; }

    public String getCity_Company() { return city_Company; }

    public String getPostCode_Company() { return postCode_Company; }

    public String getProvince_Company() { return province_Company; }

    public String getStreet_Company() { return street_Company; }

    public void setGiveName(String giveName)
    {
        this.giveName = giveName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhoneNumber(ArrayList<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(ArrayList<String> email) {
        Email = email;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setQQ(String QQ) {
        this.QQ = QQ;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setCountry_Home(String country_Home) {
        this.country_Home = country_Home;
    }

    public void setProvince_Home(String province_Home) {
        this.province_Home = province_Home;
    }

    public void setCity_Home(String city_Home) {
        this.city_Home = city_Home;
    }

    public void setStreet_Home(String street_Home) {
        this.street_Home = street_Home;
    }

    public void setPostCode_Home(String postCode_Home) {
        this.postCode_Home = postCode_Home;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public void setCountry_Company(String country_Company) {
        this.country_Company = country_Company;
    }

    public void setProvince_Company(String province_Company) {
        this.province_Company = province_Company;
    }

    public void setCity_Company(String city_Company) {
        this.city_Company = city_Company;
    }

    public void setStreet_Company(String street_Company) {
        this.street_Company = street_Company;
    }

    public void setPostCode_Company(String postCode_Company) {
        this.postCode_Company = postCode_Company;
    }
}
