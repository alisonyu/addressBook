package App.Public.Lib;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by alisonyu on 17-2-2.
 */
public class VcardReader
{
    public String seperator="&&&";

    /**
     * 返回Vcard对象
     * @param path
     * @return
     * @throws Exception
     */
    public  ArrayList<Vcard> readVcard(String path) throws Exception
    {
        ArrayList<Vcard> res=new ArrayList<>();
        ArrayList<String> textDatas=new ArrayList<>();
        try {
            textDatas=getDatas(path);
        } catch (Exception e) {
            throw e;
        }
        for(String item:textDatas)
        {
            res.add(toVcard(item));
        }
        return res;
    }

    /**
     * 对vcard文件进行初步处理
     * @param path 文件路径
     * @return
     * @throws Exception
     */
    public  ArrayList<String> getDatas(String path) throws Exception
    {
        String buffer="";
        String beginFlag="(?i)begin:vcard";
        String endFlag="(?i)end:vcard";
        ArrayList<String> datas=new ArrayList<>();
        Scanner in=new Scanner(new File(path));
        while(in.hasNext())
        {
            String text=in.nextLine();
            //如果遇到begin:vcard则开始向缓存写数据，若当时缓存中还有数据，则说明vcf的格式是不合法的
            if(text.matches(beginFlag))
            {
                if(buffer.length()!=0)
                {
                    throw new Exception("vcf格式不合法");
                }
                else
                {
                    continue;
                }
            }
            //如果遇到end：Vcard，说明一个vcard对象已经读取完毕，则将缓存中的数据加到datas中，并将缓存清空
            else if(text.matches(endFlag))
            {
                datas.add(buffer);
                buffer="";
            }
            //读取内容，在vcard格式中并没有分割符（除了换行），所以加上分割符号，便于后面的数据处理
            else
            {
                buffer+=text+seperator;
            }
        }
        return datas;
    }

    /**
     * 将文本数据转化为vcard对象（规则很多要一个个匹配）
     * @param data 文本数据
     * @return
     */
    public  Vcard toVcard(String data)
    {
        Vcard obj=new Vcard();
        String[] res=data.split(seperator);
        for(String item:res)
        {
            String[] temp=item.split(":",-1);
            String key=temp[0].toLowerCase();
            String value=temp[1];
            if(key.indexOf("fn")==0)
            {
                addFullName(obj,value);
            }
            else if(key.indexOf("n")==0&&!key.contains("nickname"))
            {
                addName(obj,value);
            }
            else if(key.indexOf("email")==0)
            {
                addEmail(obj,value);
            }
            else if(key.indexOf("tel")==0)
            {
                if(key.contains("fax"))
                {
                    addFax(obj,value);
                }
                else
                {
                    addPhone(obj,value);
                }
            }
            else if(key.indexOf("adr")==0&&key.contains("home"))
            {
                addHomeAddress(obj,value);
            }
            else if(key.indexOf("adr")==0&&key.contains("work"))
            {
                addCompanyAdress(obj,value);
            }
            else if(key.indexOf("bday")==0)
            {
                addBirthday(obj,value);
            }
            else if(key.indexOf("fax")==0)
            {
                addFax(obj,value);
            }
            else if(key.indexOf("org")==0)
            {
                addCompany(obj,value);
            }
            else if(key.indexOf("role")==0)
            {
                addJob(obj,value);
            }
            else if(key.indexOf("title")==0)
            {
                addTitle(obj,value);
            }
            else if(key.indexOf("gender")==0)
            {
                addSex(obj,value);
            }
            else if(key.indexOf("nickname")==0)
            {
                addNickName(obj,value);
            }
            else if(key.indexOf("X-QQ")==0)
            {
                addQQ(obj,value);
            }
        }
        return obj;
    }

    public void addFullName(Vcard obj, String data)
    {
        obj.setFullName(data);
    }

    public void addName(Vcard obj, String data)
    {
        String[] s=data.split(";",-1);
        obj.setGiveName(s[0]);
        obj.setFirstName(s[1]);
    }

    public void addBirthday(Vcard obj, String data)
    {
        obj.setBirthday(data);
    }

    public void addHomeAddress(Vcard obj, String data)
    {
        String[] s=data.split(";",-1);
        obj.setStreet_Home(s[0]);
        obj.setCity_Home(s[1]);
        obj.setProvince_Home(s[2]);
        obj.setPostCode_Home(s[3]);
        obj.setCountry_Home(s[4]);
    }

    public void addCompanyAdress(Vcard obj, String data)
    {
        String[] s=data.split(";",-1);
        obj.setStreet_Company(s[0]);
        obj.setCity_Company(s[1]);
        obj.setProvince_Company(s[2]);
        obj.setPostCode_Company(s[3]);
        obj.setCountry_Company(s[4]);
    }

    public void addFax(Vcard obj, String data)
    {
        obj.setFax(data);
    }

    public void addCompany(Vcard obj, String data)
    {
        obj.setCompany(data);
    }

    public void addPhone(Vcard obj, String data)
    {
        obj.getPhoneNumberList().add(data);
    }

    public void addEmail(Vcard obj, String data)
    {
        obj.getEmailList().add(data);
    }

    public  void addJob(Vcard obj, String data)
    {
        obj.setJob(data);
    }

    public void addTitle(Vcard obj, String data)
    {
        obj.setDepartment(data);
    }

    public void addSex(Vcard obj, String data)
    {
        switch(data)
        {
            case "M":
                data="男";
                break;
            case "F":
                data="女";
                break;
        }
        obj.setSex(data);
    }

    public void addNickName(Vcard obj, String data)
    {
        obj.setPetName(data);
    }

    public void addQQ(Vcard obj,String data)
    {
        obj.setQQ(data);
    }


}
