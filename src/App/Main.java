package App;

import App.Model.AuthData;
import App.Model.Contact_Message;
import App.Model.UserData;
import App.Public.Util.CipherUtil;
import App.Public.controller;
import App.Public.function;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.File;
import java.util.Scanner;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        //界面程序入口v
        function s=new function();
        s.showAPP("Pathway通讯录—登录","fxml_LoginView.fxml");
        //s.showAPP("Login v1.0.2","fxml_Mainiew.fxml");
        //s.showAPP("hello world","fxml_AddContact.fxml");
    }

    public static void createAdmin() throws Exception {
        //UserData o=(new AuthData()).testUserData("admin");
        UserData o=new UserData("admin");
        Scanner input=new Scanner(new File(System.getProperty("user.dir")+"/testData.txt"),"gbk");
        while(input.hasNextLine())
        {
            String inputLine=input.nextLine();
            String[] datas=inputLine.split(" ");
            Contact_Message m = new Contact_Message();
            m.setGiveName(datas[0]);
            m.setFullName(datas[0]);
            m.addPhoneNumber(datas[1]);
            m.addEmail(datas[2]);
            m.addGroup(datas[3]);
            o.addPerson(m);
        }
        o.save();
    }

    public static void createTestData() throws Exception
    {
        //用于生成测试数据,如果因为修改了类导致.dat文件读取不了，用这个函数重新生成数据
        File f=new File(System.getProperty("user.dir")+"/userFile/auth/authData.dat");
        f.delete();
        AuthData user = (new AuthData()).getData();
        String psw = CipherUtil.generatePassword("123456"); //加密密码123456
        user.register("admin",psw);
        user.save();
        createAdmin();
    }

    public static void test() throws Exception
    {
        controller.DATA.register("userData",new UserData());
    }


    public static void main(String[] args) throws Exception {
        //修改了userName,重新生成测试数据
        //createAdmin();
        createTestData();
        AuthData user = (new AuthData()).getData();
        controller.DATA.register("userList"); //AuthData
        controller.DATA.putData("userList",user);
        launch(args);
    }
}
