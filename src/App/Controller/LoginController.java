package App.Controller;

import App.Model.AuthData;
import App.Public.controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * LoginController
 */
public class LoginController extends controller implements Initializable
{
    @FXML public BorderPane rootPane;
    @FXML public Label label_PasswordWarn;
    @FXML public Label label_UsernameWarn;
    @FXML public TextField text_Username;
    @FXML public PasswordField text_Password;
    @FXML public Button button_Login;
    @FXML public CheckBox checkBox_AutoLogin;

    public LoginController() throws Exception {}

    /**
     * 初始化
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        label_PasswordWarn.setText("");
        label_UsernameWarn.setText("");

        text_Username.textProperty().addListener(new ChangeListener<String>() {   //动态监听器
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                label_UsernameWarn.setText("");
            }});

        text_Password.textProperty().addListener((observable, oldValue, newValue) -> {  //动态监听器
            label_PasswordWarn.setText("");
        });

        text_Password.setOnKeyPressed((event) -> {
            if(event.getCode() == KeyCode.ENTER) {
                try {
                    LoginAction();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @FXML
    public void LoginAction() throws Exception {
        String userName = text_Username.getText();
        String passWord = text_Password.getText();

        AuthData userList = (AuthData) controller.DATA.getData("userList");

        if (userName.equals("")) {
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_UsernameWarn.setText("账号不能为空！");
        }
        if (passWord.equals("")){
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_PasswordWarn.setText("密码不能为空！");
        }
        if (userList.checkPsw(userName,passWord)){
            DATA.register("userData");                              //Register userData
            DATA.putData("userData",userList.getUserData(userName));
            DATA.register("userNow");
            DATA.setData("userNow",userName);

            Stage stage = getAppStage();
            showAPP("Pathway通讯录","fxml_Mainview.fxml",new Stage());
            stage.hide();
        }
        else {
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_PasswordWarn.setText("账号或密码错误");
        }
    }

    @FXML
    public void toRegister() throws Exception
    {
        showAPP("Pathway通讯录—注册","fxml_Register.fxml",getAppStage());
    }

    public void iniCss() throws Exception {
        label_PasswordWarn.getStylesheets().add(getResource("CSS/index.css").toExternalForm());
    }

    public void fun()
    {

    }
}
