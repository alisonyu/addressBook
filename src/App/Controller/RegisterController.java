package App.Controller;

import App.Model.AuthData;
import App.Public.Util.CipherUtil;
import App.Public.controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController extends controller implements Initializable
{
    @FXML public Label label_PasswordWarn;
    @FXML public Label label_UsernameWarn;
    @FXML public TextField text_Username;
    @FXML public PasswordField text_Password;
    public RegisterController() throws Exception {}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        label_PasswordWarn.setText("");
        label_UsernameWarn.setText("");

        text_Username.textProperty().addListener(new ChangeListener<String>() {   //加了两个动态监听器
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                label_UsernameWarn.setText("");
            }});

        text_Password.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                label_PasswordWarn.setText("");
            }});

        text_Password.setOnKeyPressed((event) -> {
            if(event.getCode() == KeyCode.ENTER) {
                try {
                    register();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @FXML
    public void register() throws Exception {
        String userName = text_Username.getText();
        String passWord = text_Password.getText();

        AuthData userList = (AuthData) controller.DATA.getData("userList");

        if (userName.equals("")) {
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_UsernameWarn.setText("账号不能为空！");
        }
        else if (passWord.equals("")){
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_PasswordWarn.setText("密码不能为空！");
        }
        else if(userList.isExist(userName))
        {
            label_PasswordWarn.setTextFill(Color.rgb(201,39,30));
            label_UsernameWarn.setText("账号已经存在！");
        }
        else //注册成功
        {
            //密码加密
            passWord = CipherUtil.generatePassword(passWord);
            userList.register(userName,passWord);
            userList.save(); //注册成功后会保存到数据文件中
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("注册成功");
            alert.setHeaderText(null);
            alert.setContentText("恭喜你注册成功，确认返回登录界面");
            alert.showAndWait();
            toLogin();
        }
    }
    @FXML
    public void toLogin() throws Exception
    {
        showAPP("Pathway通讯录—登录","fxml_LoginView.fxml",getAppStage());
    }
}
