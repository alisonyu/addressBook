package App.Controller;

import App.Model.AuthData;
import App.Model.UserData;
import App.Public.Util.CipherUtil;
import App.Public.controller;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class ManagementController extends controller implements Initializable {
    public Tab tab_ModifyPassword;
    @FXML private Label label_ContactNumber;
    @FXML private Label label_UserName;
    @FXML private Label label_StarContactNumber;
    @FXML private Label label_WarnOldPassword;
    @FXML private Label label_WarnNewPassword;
    @FXML private Label label_WarnCheckNewPassword;
    @FXML private PasswordField text_OldPassword;
    @FXML private PasswordField text_NewPassword;
    @FXML private PasswordField text_CheckNewPassword;

    private UserData user=(UserData) DATA.getData("userData");

    public ManagementController() throws Exception {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        passwordAllClear();

        label_UserName.setText("  " + user.getUsername());

        label_ContactNumber.setText("  " + String.valueOf(user.getContactNumber()));

        label_StarContactNumber.setText("  " + String.valueOf(user.getStarNumber()));

        tab_ModifyPassword.selectedProperty().addListener(observable -> passwordAllClear());

        text_OldPassword.textProperty().addListener((observable, oldValue, newValue) -> label_WarnOldPassword.setText(""));

        text_NewPassword.textProperty().addListener((observable, oldValue, newValue) -> label_WarnNewPassword.setText(""));

        text_CheckNewPassword.textProperty().addListener((observable, oldValue, newValue) -> label_WarnCheckNewPassword.setText(""));
    }

    public void modifyAction() throws Exception {
        String oldPassword = text_OldPassword.getText();
        String newPassWord = text_NewPassword.getText();
        String checkNewPassword = text_CheckNewPassword.getText();

        AuthData userList = (AuthData) controller.DATA.getData("userList");

        boolean doIt = true ;

        if (!userList.checkPsw(user.getUsername(), oldPassword)) {
            label_WarnOldPassword.setTextFill(Color.rgb(201,39,30));
            label_WarnOldPassword.setText("  ❌ 密码错误");
            doIt = false ;
        }
        if (newPassWord.equals("")) {
            label_WarnNewPassword.setTextFill(Color.rgb(201,39,30));
            label_WarnNewPassword.setText("  ❌ 请输入新密码");
            doIt = false ;
        }
        if (checkNewPassword.equals("")){
            label_WarnCheckNewPassword.setTextFill(Color.rgb(201,39,30));
            label_WarnCheckNewPassword.setText("  ❌ 请确认新密码");
            doIt = false ;
        } else if ( !checkNewPassword.equals(newPassWord) ) {
            label_WarnNewPassword.setTextFill(Color.rgb(201,39,30));
            label_WarnNewPassword.setText("  ❌ 新密码不匹配");
            label_WarnCheckNewPassword.setTextFill(Color.rgb(201,39,30));
            label_WarnCheckNewPassword.setText("  ❌ 新密码不匹配");
            doIt = false ;
        }
        if (doIt) {
            String passWord = CipherUtil.generatePassword(checkNewPassword);
            userList.modifyPassword(user.getUsername(),passWord);
            userList.save();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("修改成功");
            alert.setHeaderText(null);
            alert.setContentText("密码修改成功");
            alert.showAndWait();
            passwordAllClear();
        }
    }

    private void passwordAllClear(){
        text_OldPassword.clear();
        text_NewPassword.clear();
        text_CheckNewPassword.clear();
        label_WarnOldPassword.setText("");
        label_WarnNewPassword.setText("");
        label_WarnCheckNewPassword.setText("");
    }
}
